const mongoose = require('mongoose');  

const StateSchema = new mongoose.Schema(
  {
    state: {
      type: String,      
      required: true,
    },
    initials: {
      type: String,
      required: true
    }
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model('State', StateSchema);