const mongoose = require('mongoose');

const PersonSchema = new mongoose.Schema(
  {
  name: {
    type: String,
    required: true,
  }, 
  type: {
    type: String,
    enum: ['F','J'],
    default: 'F',
    required: true,
  },
  identity: {
    type: String,
    required: true,
    unique : true,
  },    
  phone: {
    type: String,
    required: true,
  },
  brith_date: {
    type: Date,
    required: false,
  },
  city: {
    type: mongoose.Schema.Types.ObjectId, ref: 'City',      
    required: true,
  },    
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model('Person', PersonSchema);
