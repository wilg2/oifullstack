const mongoose = require('mongoose');

const CitySchema = new mongoose.Schema(
  {
  name: {
    type: String,
    required: true,
  },    
  state: {
    type: mongoose.Schema.Types.ObjectId, ref: 'State',      
    required: true,
  },    
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model('City', CitySchema);
