const City = require("../model/city");

class CityController {
  async index(req, res) {
    try {
      const cities = await City.find();
      return res.json(cities);
    } catch (err) {
      console.log(err);
      return res.status(400).json({ error: "Error list cities" });
    }
  }

  async store(req, res) {
    try {
      const { name, state } = req.body;

      const city = await City.create({
        name,
        state
      });

      return res.json(city);
    } catch (err) {
      return res.status(400).json({ error: "Error trying save to city" });
    }
  }
  async cityByState(req, res) {
    try {
      const { state } = req.params;
      const cities = await City.find({
        state
      });
      return res.json(cities);
    } catch (err) {
      return res.status(400).json({ error: "Error trying save to city" });
    }
  }

  async delete(req, res) {
    const { id } = req.params;
    const city = await City.findById(id);

    if (!city) {
      return res.status(404).json({ error: "id not found" });
    }

    await city.remove();

    return res.status(204).send();
  }
}

module.exports = new CityController();
