const Person = require("../model/person");

class PersonController {
  async index(req, res) {
    try {
      const { page = 1, limit = 5 } = req.query;

      const people = await Person.find().populate('city').limit(limit * 1).skip((page - 1) * limit).exec();
      const total =  await Person.find().populate('city').count();

      return res.json({ records: people, total });
    } catch (err) {
      console.log(err);
      return res.status(400).json({ error: "Error list people" });
    }
  }

  async show(req, res) {
    try{
      const { identity } = req.body;
      const { city } = req.params;
      const people = await Person.findOne({ identity, city}).populate('city');
      return res.json(people);
    } catch (err) {
      console.log(err);
      return res.status(400).json({ error: "Error list people" });
    }
  }

  async showById(req, res) {
    try{
      const { id } = req.params;
      const people = await Person.findById(id).populate('city');
      return res.json(people);
    } catch (err) {
      console.log(err);
      return res.status(400).json({ error: "Error list people" });
    }
  }


  async store(req, res) {
    try {
      const { name, city,type, identity, phone, brith_date } = req.body;

      const person = await Person.create({
        name, city,type, identity, phone, brith_date
      });

      return res.json(person);
    } catch (err) {
      return res.status(400).json({ error: "Error trying save to person" });
    }
  }

  async update(req, res) {
    try {
      const { id } = req.params;
      const { name, city,type, identity, phone } = req.body;

      const person = await Person.findOneAndUpdate({_id: id} , 
        { name, city,type, identity, phone}, {new: true});

        return res.json(person);
    } catch (err) {
      return res.status(400).json({ error: "Error trying save to city" });
    }
  }

  async delete(req, res) {
    const { id } = req.params;

    const person = await Person.findById(id);
    
    if (!person) {
      return res.status(404).json({ error: "id not found" });
    }

    await person.remove();

    return res.status(204).send();
  }
}

module.exports = new PersonController();
