const State = require("../model/state");

class StateController {
  async index(req, res) {
    try {

      const states = await State.find();
      return res.json(states);
    } catch (err) {
      console.log(err);
      return res.status(400).json({ error: "Error list states" });
    }
  }

  async store(req, res) {
    try {
      const { name, initials } = req.body;

      const state = await State.create({
        state:name,
        initials
      });

      return res.json(state);
    } catch (err) {
      return res.status(400).json({ error: "Error trying save to state" });
    }
  }

  async delete(req, res) {
    const { id } = req.params;
    const state = await State.findById(id);

    if (!state) {
      return res.status(404).json({ error: "id not found" });
    }

    await state.remove();

    return res.status(204).send();
  }
}

module.exports = new StateController();
