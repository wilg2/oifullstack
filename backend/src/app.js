require("dotenv").config();
const compression = require("compression");
const express = require("express");
const cors = require('cors');
const helmet = require("helmet");
const { errors } = require("celebrate");
const routes = require("./routes");
require('./databases');


class App {
  constructor() {
    this.server = express();

    this.middlewares();
    this.routes();
  }

  middlewares() {
    this.server.use(express.json());

    // compress responses
    this.server.use(compression());
    this.server.use(cors());
    this.server.use(errors());
    //Helmet helps you secure your Express apps by setting various HTTP headers.
    this.server.use(helmet())
  }

  routes() {
    this.server.use(routes);
  }
}

module.exports = new App().server;