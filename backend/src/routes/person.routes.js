const { Router } = require('express');
const { celebrate, Segments, Joi } = require("celebrate"); 

const personController = require('../app/controller/personController');
const peopleRoute = Router();

//People
peopleRoute.get('/', personController.index);
peopleRoute.get('/:id', celebrate({
  [Segments.PARAMS]: Joi.object().keys({
    id: Joi.string().required(),
  }),
}), personController.showById);
peopleRoute.post('/:city', personController.show)
peopleRoute.post('/', celebrate({
  [Segments.BODY]: Joi.object().keys({
    name: Joi.string().required(),
    city: Joi.string().required(),
    type: Joi.string().required(),
    identity: Joi.string().required(), 
    phone: Joi.string().required(), 
    brith_date: Joi.date().required()
  }),
}),personController.store);
peopleRoute.put('/:id',  celebrate({
  [Segments.BODY]: Joi.object().keys({
    name: Joi.string().required(),
    city: Joi.string().required(),
    type: Joi.string().required(),
    identity: Joi.string().required(), 
    phone: Joi.string().required(), 
  }),
  [Segments.PARAMS]: {
    id: Joi.string().required()
  }
}),personController.update);
peopleRoute.delete('/:id', celebrate({
  [Segments.PARAMS]: Joi.object().keys({
    id: Joi.string().required(),
  }),
}),personController.delete);

module.exports = peopleRoute;