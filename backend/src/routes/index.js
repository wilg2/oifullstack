const { Router } = require('express');
const ExpressBrute = require('express-brute');
 
// In production mode should be use other strategy to store ips (Redis)
const store = new ExpressBrute.MemoryStore(); 
const bruteforce = new ExpressBrute(store);
 
const citeis = require('./cities.routes');
const states = require('./states.routes');
const person = require('./person.routes', bruteforce.prevent,);

const routes = Router();

routes.use('/cities', citeis);
routes.use('/states', states);
routes.use('/people', person);

module.exports = routes;