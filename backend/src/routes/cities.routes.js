const { Router } = require('express');
const { celebrate, Segments, Joi } = require("celebrate");

const cityController = require('../app/controller/cityController')
const citiesRoute = Router();

citiesRoute.post('/', celebrate({
  [Segments.PARAMS]: Joi.object().keys({
    name: Joi.string().required(),
    state: Joi.string().required(),
  }),
}), cityController.store);
citiesRoute.get('/', cityController.index);
citiesRoute.get('/:state', celebrate({
  [Segments.PARAMS]: Joi.object().keys({
    state: Joi.string().required(),
  }),
}),cityController.cityByState);

module.exports = citiesRoute;