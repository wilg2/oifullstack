const { Router } = require('express');

const stateController = require('../app/controller/stateController')

const stateRoute = Router();

stateRoute.get('/', stateController.index);
stateRoute.post('/', stateController.store);


module.exports = stateRoute;