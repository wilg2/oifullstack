import api from "../api";

class DataServiceState {
  getAll() {
    return api.get("/states");
  }
}

export default new DataServiceState();