import api from "../api";

class DataServicePerson {
  getAll(page) {
    return api.get(`/people?page=${page}`);
  }

  getPersonByIdentity(identity, city) {
    return api.post(`/people/${city}`, {identity});
  }

  getPersonById(id) {
    return api.get(`/people/${id}`);
  }

  savePerson(data) {
    return api.post('/people', { 
      name: data.name, 
      city: data.cities, 
      type: data.type , 
      identity: data.identity, 
      phone: data.phone, 
      brith_date: data.brith_date
    })
  }

  updatePerson(id, data) {
    return api.put(`/people/${id}`, { 
      name: data.name, 
      city: data.city, 
      type: data.type , 
      identity: data.identity, 
      phone: data.phone, 
      brith_date: data.brith_date
    })
  }

  deletePerson(id) {
    return api.delete(`/people/${id}`)
  }
}

export default new DataServicePerson();