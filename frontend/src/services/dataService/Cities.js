import api from "../api";

class DataServiceCity {
  getAll() {
    return api.get("/cities");
  }

  getCitiesByState(state) {
    return api.get(`/cities/${state}`);
  }
}

export default new DataServiceCity();