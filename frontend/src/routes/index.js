import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Phones from '../pages/Phones';
import People from '../pages/People/List';
import PeopleCreate from '../pages/People/Create';

const Routes = () => (
  <Switch>
    <Route exact path="/" component={Phones}/>
    <Route exact path="/people" component={People} />
    <Route path="/people/cad" component={PeopleCreate} />
    <Route path="/people/cad/:id" component={PeopleCreate} />
  </Switch>
);

export default Routes;
