import React, { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { useForm, Controller } from 'react-hook-form';
import { Link, useParams } from 'react-router-dom';
import InputMask from 'react-input-mask';
import {
  Container,
  FormContainer,
  ActionContainer,
} from './styles';
import { Cities, State, Person } from '../../../services/dataService';

const PeopleCreate = () => {
  const { register, handleSubmit, watch, errors, setValue, control, reset } = useForm();
  const [result, setResult] = useState(null);
  const [states, setStates] = useState([]);
  const [cities, setCities] = useState([]);
  const [typeMask, setTypeMask] = useState('999.999.999-99')
  const { id } = useParams();

  const fetchData = async () => {
    if (id) {
      const response = await Person.getPersonById(id);
      const { name, identity, phone, brith_date, city } = response.data;
      setValue([{ name, identity, phone, brith_date, city }]);
    }
    const responseState = await State.getAll();
    setStates(responseState.data);
  };

  const fetchCities = async (state) => {
    const response = await Cities.getCitiesByState(state);
    setCities(response.data);
  };
  useEffect(() => {
    fetchData();    
  }, [id]);

  const handleChangeCity = (state) => {
    fetchCities(state);
  }

  const onSubmit = async (data, e) => {
    try {
      let response = null;
      if (id) {
        response = await Person.updatePerson(id, data);
      } else {
        response = await Person.savePerson(data);
      }

      if (response.status === 200) {
        reset()
        toast.success('Registro salvo com sucesso');
      }
      
    } catch (err) {
      console.log(err);
      toast.error('Erro ao tentar salvar a pessoa');
    }
  };

  return (
    <Container>
      <FormContainer>
        <h1>Gerenciando Pessoas</h1>
        <form onSubmit={handleSubmit(onSubmit)} data-testid="submit">
          <div>
            <input type="radio" id="fisica" ref={register({
                required: 'Campo obrigatório',
              })} name="type" value="F"/>
            <label for="fisica">Pessoa Física</label> 
            <input type="radio" id="juridica" ref={register({
                required: 'Campo obrigatório',
              })} name="type" value="J"/>
            <label for="juridica">Pessoa Jurídica</label> 
            <span>{errors.type && errors.type.message}</span>
          </div>
          <div>
            <input
              name="name"
              type="text"
              ref={register}
              placeholder="Entre com nome"
              ref={register({
                required: 'Campo obrigatório',
              })}
            />
            <span>{errors.name && errors.name.message}</span>
          </div>
          <div>
            <Controller
              as={InputMask}
              control={control} 
              mask={typeMask}
              name="identity"
              type="text"
              placeholder="Entre com identificação"
              ref={register({
                required: 'Campo obrigatório',
              })}
            />
            <span>{errors.identity && errors.identity.message}</span>
          </div>
          <div>
            <select name="states" ref={register} data-testid="states" onChange={(event) => handleChangeCity(event.target.value)}>
              <option value="">Selecione UF</option>
              {states.map((value) => (
                <option value={value._id}>{value.initials}</option>
              ))}
            </select>
          </div>
          <div>
            <select name="cities" ref={register} data-testid="cities" onChange={handleChangeCity}>
              {cities.map((value) => (
                <option value={value._id}>{value.name}</option>
              ))}
            </select>
          </div>
          {!id && (
            <div>
            <input
              name="brith_date"
              type="date"
              ref={register}
              ref={register({
                required: 'Campo obrigatório',
              })}
            />
            <span>{errors.brith_date && errors.brith_date.message}</span>
          </div>
          )}
          
          <div>
            <Controller
              as={InputMask}
              control={control} 
              mask="(99) 9999-99999"
              name="phone"
              type="text"
              placeholder="Entre com Telefone"
              ref={register({
                required: 'Campo obrigatório',
              })}
            />  
            <span>{errors.phone && errors.phone.message}</span>
          </div>
          <ActionContainer>
            <button type="submit">SALVAR</button>
            <Link to="/people">Voltar para lista de Pessoas</Link>
          </ActionContainer>
        </form>
      </FormContainer>
    </Container>
  );
};

export default PeopleCreate;