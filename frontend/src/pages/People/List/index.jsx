import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { FaTrashAlt, FaPencilAlt } from "react-icons/fa";
import { MdSkipPrevious, MdSkipNext} from 'react-icons/md';
import { IoIosArrowForward, IoIosArrowBack} from 'react-icons/io'

import {
  Container,
  Header,
  Table
} from './styles';
import { Person } from '../../../services/dataService';
import { toast } from 'react-toastify';

const PeopleList = () => {
  const [people, setPeople] = useState(null);
  const [loading, setLoading] = useState(true);
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(0);

  const fetchData = async () => {
    setLoading(true)
    const response = await Person.getAll(page);
    setPeople(response.data.records);
    setTotal(response.data.total)
    setLoading(false)
  };

  useEffect(() => {
    fetchData();    
  }, [page]);

  const handleDelete = async (id) => {
    try {
      await Person.deletePerson(id);
      toast.success('Item excluído com sucesso.')      
    } catch (err) {
      toast.error('Erro ao tentar excluir')
    }
  }
  
  const handlePage = (currentPage) => {
    if (currentPage >= 1) {
      setPage(currentPage)
    }
  }

  return (
    <Container>
        <Header>
          <h1>Lista de Pessoas</h1> <Link to="/people/cad" >Criar Nova Pessoas</Link>
        </Header>
        <Table>
          <thead>
            <tr>
              <th scope="col">Tipo</th>
              <th scope="col">Nome/ Razao Social</th>
              <th scope="col">CPF/ CNPJ</th>
              <th scope="col">Telefone</th>
              <th scope="col">Cidade</th>
              <th scope="col">Ações</th>
            </tr>
          </thead>
          <tbody>
            {people && people.length === 0 && (
              <tr>
                <td colspan="6">Nenhum registro encontrado </td>
              </tr>
            )}

            {!loading && people.map(item => (
              <tr key={item._id }>
                <td>{item.type === 'F' ? 'Física': 'Jurídica'}</td>
                <td>{item.name}</td>
                <td>{item.identity}</td>
                <td>{item.phone}</td>
                <td>{item.city.name}</td>
                <td><Link to={`/people/cad/${item._id}`}><FaPencilAlt /></Link><FaTrashAlt onClick={() => handleDelete(item._id)}/></td>
              </tr>
            ))}
            
          </tbody>
        </Table>
        <div>
        <MdSkipPrevious onClick={()=> handlePage(1)} size={25}/>
        <IoIosArrowBack onClick={()=> handlePage(page-1)}  size={25}/>
        {page} de {total}
        <IoIosArrowForward onClick={()=> handlePage(page+1)}  size={25}/> 
        <MdSkipNext onClick={()=> handlePage(Math.ceil(total/5))} size={25}/>
        </div>
        <Link to="/">Voltar para tela de pesquisa de Telefones</Link>
    </Container>
  );
};

export default PeopleList;