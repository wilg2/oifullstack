import styled from 'styled-components';
import { darken } from 'polished';

export const Container = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: #d6d7e6;
  @media only screen and (max-width: 600px) {
    flex-direction: column;
  }

  a {
    color: var(--twitter);
    text-decoration: none;
    font-weight: bold;
    margin-top: 15px;
  }
`;

export const Header = styled.div`
  display: flex;
  flex-direction: row;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
`

export const Table = styled.table`
  border-collapse: collapse;
  margin: 25px 0;
  font-size: 0.9em;
  font-family: sans-serif;
  min-width: 400px;
  box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
`;