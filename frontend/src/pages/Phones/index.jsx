import React, { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { useForm, Controller } from 'react-hook-form';
import { Link } from 'react-router-dom';
import InputMask from 'react-input-mask';

import {
  Container,
  FormContainer,
  BillContainer,
  ActionContainer,
} from './styles';
import { Cities, State, Person } from '../../services/dataService';

const Phones = () => {
  const { register, handleSubmit, watch, errors, setValue, control } = useForm();
  const [result, setResult] = useState(null);
  const [states, setStates] = useState([]);
  const [cities, setCities] = useState([]);
  const [typeMask, setTypeMask] = useState('999.999.999-99')

  const fetchData = async () => {
    const response = await State.getAll();
    setStates(response.data);
  };

  const fetchCities = async (state) => {
    const response = await Cities.getCitiesByState(state);
    setCities(response.data);
  };
  useEffect(() => {
    fetchData();    
  }, []);

  const handleChangeCity = (state) => {
    fetchCities(state);
  }

  const onSubmit = async (data, e) => {
    try {
      const response = await Person.getPersonByIdentity(data.identity, data.cities);

      if (response.status === 200) {
        if (!response.data) {
          toast.warning('Nenhum registro encontrado')
        }

        setResult(response.data)
      }
    } catch (err) {
      console.log(err);
      toast.error('Erro na simulação do plano!');
    }
  };

  return (
    <Container>
      <FormContainer>
        <h1>Lista Telefones</h1>
        <form onSubmit={handleSubmit(onSubmit)} data-testid="submit">
          <div>
            <input type="radio" id="fisica" name="type" ref={register({
                required: 'Campo obrigatório',
              })} value="F" onChange={() => setTypeMask('999.999.999-99')} />
            <label for="fisica">Pessoa Física</label> 
            <input type="radio" id="juridica" name="type" ref={register({
                required: 'Campo obrigatório',
              })} value="J" onChange={() => setTypeMask('99.999.999/9999-99')}/>
            <label for="juridica">Pessoa Jurídica</label> 
            <span>{errors.type && errors.type.message}</span>
          </div>
          <div>
            <Controller
              as={InputMask}
              control={control} 
              mask={typeMask}
              name="identity"
              type="text"
              placeholder="Entre com identificação"
              ref={register({
                required: 'Campo obrigatório',
              })}
            />
            <span>{errors.identity && errors.identity.message}</span>
          </div>
          <div>
            <select name="states" ref={register({
                required: 'Campo obrigatório',
              })} data-testid="states" onChange={(event) => handleChangeCity(event.target.value)}>
              <option value="">Selecione UF</option>
              {states.map((value) => (
                <option value={value._id}>{value.initials}</option>
              ))}
            </select>
            <span>{errors.states && errors.states.message}</span>
          </div>
          <div>
            <select name="cities" ref={register({
                required: 'Campo obrigatório',
              })} data-testid="cities" onChange={handleChangeCity}>
              {cities.map((value) => (
                <option value={value._id}>{value.name}</option>
              ))}
            </select>
            <span>{errors.cities && errors.cities.message}</span>
          </div>
          <ActionContainer>
            <button type="submit">BUSCAR</button>
            <Link to="/people">Gerenciar Pessoas</Link>
          </ActionContainer>
        </form>
      </FormContainer>
      <BillContainer>
        <h1>Resultado</h1>
        {result ?  (
          <>
          <span>{result.name}</span>
          <span>{result.identity}</span>
          <span>{result.city.name}</span>
          <span>{result.phone}</span>
          </>
        ): (<p>Nenhum Resultado</p>)}        
      </BillContainer>
    </Container>
  );
};

export default Phones;