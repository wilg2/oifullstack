# TESTE

## Instalando

Obs.: É necessário antes de instalar este projeto, ter instalado e rodando node.js:

- Nodejs (https://nodejs.org/en/)

para fazer a instalação das dependências do projeto você pode utilizar o yarn ou utilizar npm

Para instalá-lo em sua máquina faça os comandos a seguir:

```bash
  git clone https://wilg2@bitbucket.org/wilg2/oifullstack.git
  cd oifullstack
```

## Baixando dependências

acesse a pasta backend e execute o comando:

```bash
    cd backend
    yarn ou npm install
    yarn start ou npm run start
```

## Baixando dependências

fazer uma cópia do .env.example e alterar os dados:

```bash
    cp .env.example .env
```

## Executando o projeto

Após executar o camando na pasta backend faça o mesmo para frontend:

```bash
    cd backend
    yarn ou npm install
    yarn start ou npm run start
```

### Docker

Dentro da pasta do backend execute o comando

```bash
docker-compose build
docker-compose up
```

Após executar os comando já deve ser possivel acessar a api usando o endereço [http://localhost:3333](http://localhost:3000)

### Backend Ferramentas e Bibliotecas

### Padrão de Código

- ESLint
- Prettier

### Gerenciando variáveis ambiente

- Dotenv

### Utilitários

-Nodemon
-Cors

### Segurança

-Helmet
-Express-brute

### Container

-Docker

### Backend

-Node.js
-Express

### Frontend

-ReactJS
-styled-components

### Aplicação

Algumas prints de toda aplicação

![](./tmp/prints/image1.png)

![](./tmp/prints/image2.png)

![](./tmp/prints/image3.png)

![](./tmp/prints/image4.png)

![](./tmp/prints/image5.png)

![](./tmp/prints/image6.png)

![](./tmp/prints/image7.png)

![](./tmp/prints/image8.png)
